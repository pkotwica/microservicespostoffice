#!/bin/bash

IMAGE_NAME_APP="monolith-app:latest"
CONTAINER_NAME_APP="monolith-app"
IMAGE_NAME_DATA="monolith-database:latest"
CONTAINER_NAME_DATA="monolith-database"
JAR_FILENAME="monolith-0.0.1-SNAPSHOT.jar"
JAR_PATH="../target/$JAR_FILENAME"
TEMP_PATH="./spring/tmp"
TEMP_JAR_FILE="$TEMP_PATH/$JAR_FILENAME"
COMPOSE_PROJECT_NAME="Monolith"

build_flag=0
stop_container=0
only_start_containers=0

while getopts ":bso" option; do
  case $option in
  b) build_flag=1 ;;
  s) stop_container=1 ;;
  o) only_start_containers=1 ;;
  *) ;;
  esac
done

if [ $stop_container -eq 1 ]; then
  printf "\n\nSTOPPING CONTAINERS: $CONTAINER_NAME\n"
  docker stop $CONTAINER_NAME_APP
  docker stop $CONTAINER_NAME_DATA
else
  printf "\n\nTo stop old container use (-s) option.\n"
fi

printf "\n\ndocker container prune\n"
docker container prune -f

if [ $build_flag -eq 1 ] && [ $only_start_containers -eq 0 ]; then
  (
    cd ../
    mvn clean package -DskipTests=true
  )

  printf "\n\nBUILDING IMAGES: $IMAGE_NAME\n"
  mkdir $TEMP_PATH
  cp $JAR_PATH $TEMP_JAR_FILE
  printf "\n\nTo start without building images - (-o)\n\n"
  docker-compose -p $COMPOSE_PROJECT_NAME up --build
  rm -r $TEMP_PATH
fi

if [ $build_flag -eq 0 ] && [ $only_start_containers -eq 1 ]; then
  mkdir $TEMP_PATH
  cp $JAR_PATH $TEMP_JAR_FILE
  printf "\n\nContainers starts without building image! (-b to build)\n\n"
  docker-compose -p $COMPOSE_PROJECT_NAME up
  rm -r $TEMP_PATH
fi

