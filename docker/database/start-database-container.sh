#!/bin/bash

IMAGE_NAME="monolith-database:latest"
CONTAINER_NAME="monolith-database"

build_flag=0
stop_container=0

while getopts ":bs" option; do
  case $option in
  b) build_flag=1 ;;
  s) stop_container=1 ;;
  *) ;;
  esac
done

if [ $stop_container -eq 1 ]; then
  printf "\n\nSTOPPING CONTAINER: $CONTAINER_NAME\n"
  docker stop $CONTAINER_NAME
else
  printf "\n\nTo stop old container use (-s) option.\n"
fi

printf "\n\ndocker container prune\n"
docker container prune -f

if [ $build_flag -eq 1 ]; then
  printf "\n\nBUILDING IMAGE: $IMAGE_NAME\n"
  docker image prune -f
  docker build . -t $IMAGE_NAME --rm=true
else
  printf "\n\nContainer starts without building image! (-b to build)\n"
fi

docker run --name $CONTAINER_NAME -dp 9000:3306 $IMAGE_NAME
