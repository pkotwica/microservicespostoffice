package pl.kotwica.postoffice.microservices.repository;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Component
public class RestTemplateRepository {

    private static final String APPLICATION_JSON = "application/json";
    private final RestTemplate restTemplate = new RestTemplate();


    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse get(String url, ParameterizedTypeReference<T> responseType, G body) {
        return exchangeRequest(url, HttpMethod.GET, new LinkedMultiValueMap<>(), body, responseType, new HashMap<>());
    }

    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse get(String url, ParameterizedTypeReference<T> responseType, G body, MultiValueMap<String, String> additionalHeaders) {
        return exchangeRequest(url, HttpMethod.GET, additionalHeaders, body, responseType, new HashMap<>());
    }

    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse get(String url, ParameterizedTypeReference<T> responseType, G body, Map<String, String> uriVariables) {
        return exchangeRequest(url, HttpMethod.GET, new LinkedMultiValueMap<>(), body, responseType, uriVariables);
    }

    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse get(String url, ParameterizedTypeReference<T> responseType, G body, MultiValueMap<String, String> additionalHeaders, Map<String, String> uriVariables) {
        return exchangeRequest(url, HttpMethod.GET, additionalHeaders, body, responseType, uriVariables);
    }

    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse post(String url, ParameterizedTypeReference<T> responseType, G body) {
        return exchangeRequest(url, HttpMethod.POST, new LinkedMultiValueMap<>(), body, responseType, new HashMap<>());
    }

    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse post(String url, ParameterizedTypeReference<T> responseType, G body, MultiValueMap<String, String> additionalHeaders) {
        return exchangeRequest(url, HttpMethod.POST, additionalHeaders, body, responseType, new HashMap<>());
    }

    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse post(String url, ParameterizedTypeReference<T> responseType, G body, Map<String, String> uriVariables) {
        return exchangeRequest(url, HttpMethod.POST, new LinkedMultiValueMap<>(), body, responseType, uriVariables);
    }

    public <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse post(String url, ParameterizedTypeReference<T> responseType, G body, MultiValueMap<String, String> additionalHeaders, Map<String, String> uriVariables) {
        return exchangeRequest(url, HttpMethod.POST, additionalHeaders, body, responseType, uriVariables);
    }

    private <T extends CommonRestTemplateResponse, G> CommonRestTemplateResponse exchangeRequest(String url, HttpMethod method, MultiValueMap<String, String> headers, G body, ParameterizedTypeReference<T> responseType, Map<String, String> uriVariables) {
        HttpEntity<G> httpEntity = createHttpEntity(headers, body);
        return processResponse(restTemplate.exchange(url, method, httpEntity, responseType, uriVariables));
    }

    private <T> HttpEntity<T> createHttpEntity(MultiValueMap<String, String> headers, T body) {
        getEssentialsHeaders().forEach((key, value) -> value.forEach(v -> headers.add(key, v)));
        return new HttpEntity<>(body, headers);
    }

    private MultiValueMap<String, String> getEssentialsHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON);
        return headers;
    }

    private <T extends CommonRestTemplateResponse> CommonRestTemplateResponse processResponse(ResponseEntity<T> responseEntity) {
        if (responseEntity.getStatusCode().equals(HttpStatus.OK)) {
            return responseEntity.getBody();
        } else {
            return CommonRestTemplateResponse.internalFailure(responseEntity.getStatusCodeValue());
        }

    }
}
