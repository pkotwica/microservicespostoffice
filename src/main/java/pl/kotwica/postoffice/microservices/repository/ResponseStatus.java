package pl.kotwica.postoffice.microservices.repository;

public enum ResponseStatus {
    SUCCESS, FAILURE, INTERNAL_FAILURE
}
