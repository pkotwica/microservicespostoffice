package pl.kotwica.postoffice.microservices.repository;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CommonRestTemplateResponse implements Serializable {
    private String status;
    private Integer httpStatus;

    public static CommonRestTemplateResponse internalFailure(Integer httpStatus) {
        return new CommonRestTemplateResponse(ResponseStatus.INTERNAL_FAILURE.name(), httpStatus);
    }
}
